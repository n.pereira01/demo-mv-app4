package com.demo.dev.services;

import com.demo.dev.models.Dolar;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Esta clase define los servicios realacionados con la Uf
 * @author Nicolas Pereira Jose Gutierrez
 */
@Service
public class DolarService {
    private final String URL = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=PRE_TCO&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAdwA1ADQAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";
    
    public List<Dolar> getAllDolarData() throws IOException {
        List<Dolar> data = new ArrayList<>();

        Document doc = Jsoup.connect(URL).get();

        // Obtener la tabla
        Elements table = doc.select("table[id=gr]");
        // Obtener los tr
        Elements tr = table.select("tr");

        for(int i=1; i<tr.size(); i++) {
            for(int j=1; j<(tr.get(i).select("td").size()-1); j++) {
                // verificar si la celda esta vacia o no
                if(!tr.get(i).select("td").get(j).hasText()) {
                    continue;
                }
                // almacenar el valor del dolar
                String valueDolar = tr.get(i).select("td").get(j).text();
                String date = "";
                String anio = "2021";
                String mes = "";
                String dia = "";

                if(j < 10) {
                    mes = "0"+j;
                }else {
                    mes = ""+j;
                }

                if(i < 10) {
                    dia = "0"+i;
                }else {
                    dia = ""+i;
                }
                date = anio + "-" + mes + "-" + dia;

                data.add(new Dolar(valueDolar, LocalDate.parse(date)));
            }
        }

        return data;
    }

    /**
     * El metodo busca un objeto de tipo Dolar por su atributo "date"
     * @param list recibe la lista de Dolar
     * @param dateSearch recibe la fecha que desea buscar
     * @return el objeto encontrado
     */
    public Dolar findDolarData(List<Dolar> list, String dateSearch) {
        Dolar dolarSearch = new Dolar();

        for(Dolar item : list) {
            if(item.getDate().isEqual(LocalDate.parse(dateSearch))) {
                dolarSearch.setValueDolar(item.getValueDolar());
                dolarSearch.setDate(item.getDate());
                break;
            }
        }

        if(dolarSearch.getValueDolar().isEmpty()) {
            throw new NullPointerException("El objeto es nulo");
        }

        return dolarSearch;
    }
}
