package com.demo.dev.services;

import com.demo.dev.models.Uf;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Esta clase define los servicios realacionados con la Uf
 * @author Nicolas Pereira
 */
@Service
public class UfService {
    private final String URL = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";
    private LocalDate localDate;

    /**
     * Obtener los datos de la UF de la URL
     * @return Lista de objetos de tipo Uf
     * @throws IOException
     */
    public List<Uf> allUfData() throws IOException {
        List<Uf> data = new ArrayList<>();
        Document doc = Jsoup.connect(URL).get();

        // Obtener la tabla
        Elements table = doc.select("table[id=gr]");
        // Obtener los tr
        Elements tr = table.select("tr");

        for(int i=1; i<tr.size(); i++) {
            for(int j=1; j<(tr.get(i).select("td").size()-1); j++) {
                // verificar si la celda esta vacia o no
                if(!tr.get(i).select("td").get(j).hasText()) {
                    continue;
                }

                String valueUf = tr.get(i).select("td").get(j).text();
                String date = "";
                String anio = "2021";
                String mes = "";
                String dia = "";

                if(j < 10) {
                    mes = "0"+j;
                }else {
                    mes = ""+j;
                }

                if(i < 10) {
                    dia = "0"+i;
                }else {
                    dia = ""+i;
                }
                date = anio + "-" + mes + "-" + dia;

                data.add(new Uf(valueUf, LocalDate.parse(date)));
            }
        }

        return data;
    }


    /**
     * El metodo busca un objeto de tipo Uf por su atributo "date"
     * @param list recibe la lista de Uf
     * @param dateSearch recibe la fecha que desea buscar
     * @return el objeto encontrado
     */
    public Uf findUfData(List<Uf> list, String dateSearch) {
        Uf ufSearch = new Uf();

        for(Uf item : list) {
            if(item.getDate().isEqual(LocalDate.parse(dateSearch))) {
                ufSearch.setValueUf(item.getValueUf());
                ufSearch.setDate(item.getDate());
                break;
            }
        }


        return ufSearch;
    }
}
