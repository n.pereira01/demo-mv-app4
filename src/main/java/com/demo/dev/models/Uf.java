package com.demo.dev.models;

import java.time.LocalDate;

public class Uf {
    private String valueUf;
    private LocalDate date;

    public Uf() {}

    public Uf(String valueUf, LocalDate date) {
        this.valueUf = valueUf;
        this.date = date;
    }

    public String getValueUf() {
        return valueUf;
    }

    public void setValueUf(String valueUf) {
        this.valueUf = valueUf;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
