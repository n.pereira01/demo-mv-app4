package com.demo.dev.models;

import java.time.LocalDate;

public class Dolar {
    private String valueDolar;
    private LocalDate date;

    public Dolar() {}

    public Dolar(String valueDolar, LocalDate date) {
        this.valueDolar = valueDolar;
        this.date = date;
    }

    public String getValueDolar() {
        return valueDolar;
    }

    public void setValueDolar(String valueDolar) {
        this.valueDolar = valueDolar;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
