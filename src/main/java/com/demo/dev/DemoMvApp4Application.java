package com.demo.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMvApp4Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoMvApp4Application.class, args);
	}

}
