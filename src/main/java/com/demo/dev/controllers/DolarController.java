package com.demo.dev.controllers;

import com.demo.dev.models.Dolar;
import com.demo.dev.services.DolarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/dolar")
public class DolarController {
    @Autowired
    private DolarService dolarService;

    @GetMapping("/all")
    public ResponseEntity<?> getAllDolarData() {
        Map<String, Object> response = new HashMap<>();
        List<Dolar> data;

        try {
            data = dolarService.getAllDolarData();
            response.put("data", data);
            response.put("size", data.size());
        }catch (IOException e) {
            response.put("error", e.getMessage());
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Controller que buscar el valor del dolar
     * @param date fecha que desea buscar (Ej. /search?date=2021-05-13)
     * @return response con los resultados de busqueda
     */
    @GetMapping("/search")
    public ResponseEntity<?> searchDolarValue(
            @RequestParam(name = "date", required = false, defaultValue = "2021-02-01") String date)
    {
        Map<String, Object> response = new HashMap<>();

        try {
            List<Dolar> dolarList = dolarService.getAllDolarData();
            if(date.isEmpty()) {
                response.put("error", "No hay fecha de busqueda");
            }else {
                Dolar dolar = dolarService.findDolarData(dolarList, date);
                response.put("search", dolar);
            }
        }catch(NullPointerException v){
            response.put("error", "Valor no encontrado");
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }catch (IOException e) {
            response.put("error", e.getCause());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
