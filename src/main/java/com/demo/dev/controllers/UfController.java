package com.demo.dev.controllers;

import com.demo.dev.models.Uf;
import com.demo.dev.services.UfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Es el RestController que esta relacionado con la obtencion y busqueda de la Uf
 */
@RestController
@RequestMapping("/uf")
public class UfController {
    @Autowired
    private UfService ufService;

    /**
     * Obtener todos log objetos de tipo Uf
     * @return mostrar todos log objetos de tipo Uf
     */
    @GetMapping("/all")
    public ResponseEntity<?> findAllData() {
        Map<String, Object> response = new HashMap<>();
        List<Uf> data;

        try {
            data = ufService.allUfData();
            response.put("data", data);
            response.put("size", data.size());
        }catch(IOException e) {
            response.put("error", e.getCause());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Buscar mediante el atributo date un objeto de tipo Uf
     * @param date es la fecha que debe ingresar por parametros en la URL
     * @return mostrar el objeto encontrado
     */
    @GetMapping("/search")
    public ResponseEntity<?> search_getUf(@RequestParam("date") String date) {
        Map<String, Object> response = new HashMap<>();

        try {
            List<Uf> ufList = ufService.allUfData();
            if(date.isEmpty()) {
                response.put("error", "No hay fecha de busqueda");
            }else {
                Uf uf = ufService.findUfData(ufList, date);
                response.put("search", uf);
            }
        }catch (IOException e) {
            response.put("error", e.getCause());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
