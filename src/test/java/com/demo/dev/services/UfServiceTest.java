package com.demo.dev.services;

import com.demo.dev.models.Uf;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UfServiceTest {
    private final UfService ufService = new UfService();

    /**
     * Test para comprobar el total de objetos cargados
     * @result es la verificacion de la cantidad de resultado obtenidos
     */
    @Test
    void allUfData_test() {
        try {
            List<Uf> data = ufService.allUfData();
            Assertions.assertEquals(282, data.size());
        }catch(IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Econtrar el valode de la UF
     * @result es la verificacion del valor de la UF con la fecha ingresada
     */
    @Test
    void findUfData_test() {
        String date = "2021-01-01";
        String valueExpected = "29.069,39";

        try{
            List<Uf> data = ufService.allUfData();
            Uf ufSearch = ufService.findUfData(data, date);

            Assertions.assertEquals(valueExpected, ufSearch.getValueUf());
        }catch(IOException e) {
            System.out.println(e.getMessage());
        }
    }
}