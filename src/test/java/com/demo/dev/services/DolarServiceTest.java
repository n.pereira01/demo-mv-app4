package com.demo.dev.services;

import com.demo.dev.models.Dolar;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DolarServiceTest {
    private final DolarService dolarService = new DolarService();

    /**
     * Obtener la data completa
     */
    @Test
    void getAllDolarData_test() {
        try {
            List<Dolar> data = dolarService.getAllDolarData();
            Assertions.assertEquals(187, data.size());
        }catch(IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Verificar que un objeto no sea nulo
     */
    @Test
    void checkNullData_test() {
        boolean correctData = true;

        try {
            List<Dolar> data = dolarService.getAllDolarData();
            for(Dolar item : data) {
                if(item.getValueDolar().isEmpty() || item.getDate() == null) {
                    correctData = false;
                    break;
                }
            }
            Assertions.assertTrue(correctData);

        }catch(IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Buscar un objeto Dolar por su atributo date, con una fecha incorrecta
     * retornara excepcion donde el Objeto encontrado es null
     */
    @Test
    void findDolarData_test() {
        String searchDate = "2021-01-01";

        try {
            List<Dolar> data = dolarService.getAllDolarData();

            Assertions.assertThrows(NullPointerException.class, () -> {
               Dolar dolar = dolarService.findDolarData(data, searchDate);
            });
        }catch(IOException e) {
            System.out.println(e.getMessage());
        }
    }
}